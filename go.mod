module elastic_search_example

go 1.16

require (
	github.com/fortytw2/leaktest v1.3.0 // indirect
	github.com/go-chi/chi v1.5.4
	github.com/google/go-cmp v0.5.5 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/olivere/elastic v6.2.35+incompatible
	github.com/olivere/elastic/v7 v7.0.23 // indirect
	github.com/pkg/errors v0.9.1 // indirect
)
