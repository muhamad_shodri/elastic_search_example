package main

import (
	"elastic_search_example/Controller"
	chi2 "github.com/go-chi/chi"
	"net/http"
)

func main() {
	chi := chi2.NewRouter()
	chi.Get("/region/search/{keyword}", Controller.GetRegion)
	chi.Post("/region/bulk_data", Controller.BulkData)

	http.ListenAndServe("localhost:3000", chi)
}
