package Controller

import (
	"context"
	"elastic_search_example/model"
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/olivere/elastic/v7"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

var (
	formatIndex string = `{"index":{"_id":"%d"}}\n`
	client      *elastic.Client
)

func init() {
	var err error
	client, err = elastic.NewClient(
		elastic.SetURL("http://127.0.0.1:9200"),
		elastic.SetSniff(false),
		elastic.SetErrorLog(log.New(os.Stderr, "ELASTIC ", log.LstdFlags)),
		elastic.SetInfoLog(log.New(os.Stdout, "", log.LstdFlags)))
	if err != nil {
		log.Println(err)
		return
	}
}

func GetRegion(w http.ResponseWriter, r *http.Request) {
	keyword := chi.URLParam(r, "keyword")
	if keyword == "" {
		responseError(w, r, "Keyword must be filled", http.StatusBadRequest)
		return
	}
	q := elastic.NewMultiMatchQuery(keyword, "city_name", "province_name", "district_name", "village_name")
	q.Fuzziness("AUTO")
	q.Operator("AND")
	search := elastic.NewSearchSource().Query(q)
	searchSource := client.Search().Index("logkar-apps").Source(search)
	searchResult, err := searchSource.Do(context.Background())
	if err != nil {
		responseError(w, r, "Error", http.StatusInternalServerError)
		log.Println(err)
		log.Println("test")
		return
	}
	var dataSource []json.RawMessage
	for _, val := range searchResult.Hits.Hits {
		dataSource = append(dataSource, val.Source)
	}
	data := map[string]interface{}{}
	data["data"] = dataSource
	responseSuccess(w, r, data)
}

//func worker(data []map[string]interface{}, dataJson chan string) {
//	var res string
//	for _, val := range data {
//		res += fmt.Sprintf(formatIndex, val["_index"])
//		delete(val, "_id")
//		resJson, _ := json.Marshal(val)
//		res += string(resJson) + "\n"
//	}
//	dataJson <- res
//}
//
//func BulkData(w http.ResponseWriter, r *http.Request) {
//	var data []map[string]interface{}
//	json.NewDecoder(r.Body).Decode(&data)
//	totalWorker := 20
//	totalData := len(data)
//
//	dataJsons := make(chan string, totalWorker)
//	for i := 0; i < totalWorker; i++ {
//		go worker(data[], dataJsons)
//	}
//
//	var resJson string
//	for dataJson := range dataJsons {
//		resJson += dataJson
//	}
//
//	fmt.Println(resJson)
//}

func BulkData(w http.ResponseWriter, r *http.Request) {
	var data []map[string]interface{}
	json.NewDecoder(r.Body).Decode(&data)
	ctx := context.Background()

	bulkRequest := client.Bulk()
	for _, val := range data {
		id := strconv.FormatFloat(val["_id"].(float64), 'f', -1, 64)
		delete(val, "_id")
		doc := elastic.NewBulkIndexRequest().Index("region-logkar-apps").Id(id).Doc(val)
		bulkRequest = bulkRequest.Add(doc)
	}

	bulkResponse, err := bulkRequest.Do(ctx)
	if err != nil {
		log.Print(err)
		responseError(w, r, err.Error(), http.StatusInternalServerError)
		return
	}

	responseSuccess(w, r, fmt.Sprint("Success total data: ", len(bulkResponse.Items)))
}

func responseError(w http.ResponseWriter, r *http.Request, data interface{}, code int) {
	location, _ := time.LoadLocation("Asia/Jakarta")
	setResponse := model.ResponseJSON{
		Status:     http.StatusText(code),
		AccessTime: time.Now().In(location).Format("02-01-2006 15:04:05"),
		Data:       data,
		Code:       code}
	response, _ := json.Marshal(setResponse)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

func responseSuccess(w http.ResponseWriter, r *http.Request, data interface{}) {
	location, _ := time.LoadLocation("Asia/Jakarta")
	responseData := model.ResponseJSON{
		Code:       http.StatusOK,
		Status:     http.StatusText(http.StatusOK),
		Data:       data,
		AccessTime: time.Now().In(location).Format("02-01-2006 15:04:05")}
	response, _ := json.Marshal(responseData)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}
